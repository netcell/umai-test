import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import React from 'react';
import { render } from 'react-dom';
import InterestForm from './components/InterestForm/InterestForm';
import InterestTable from './components/InterestTable/InterestTable';
import { Grid, Row, Col } from 'react-bootstrap'
import { Provider } from 'react-redux'
import configureStore from './redux/configureStore'

const App = () => (
  <Provider store={configureStore()}>
    <Grid fluid>
      <Row>
        <Col xs={12} sm={5} md={4}>
          <InterestForm/>
        </Col>
        <Col xs={12} sm={7} md={8}>
          <InterestTable/>
        </Col>
      </Row>
    </Grid>
  </Provider>
);

render(<App />, document.getElementById('root'));
