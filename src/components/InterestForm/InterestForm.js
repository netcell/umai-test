import React from 'react'
import _ from 'lodash';
import { Nav, NavItem, FormControl, FormGroup, ControlLabel, HelpBlock, Grid } from 'react-bootstrap'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import InterestInput from './InterestInput'
import InterestTypeNav from '../InterestTypeNav/InterestTypeNav'

import { get as getInterestType } from '../../redux/InterestType/InterestTypeRedux'
import interestFields from '../../redux/interestFields'

const mapStateToProps = state => {
  const interestType = getInterestType(state);
  return {
    interestFields: interestFields(interestType)
  }
}

class InterestForm extends React.Component {

  render() {
    return <div>
      <center>
        <h1>Interest Calculator</h1>
      </center>
      <InterestTypeNav />
      <div style={{ marginTop: 10 }}>
        { this.props.interestFields.map(field => (
            <InterestInput key={field.id} {...field} />
        )) }
      </div>
    </div>
  }
}

export default connect(mapStateToProps)(InterestForm)