import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { FormControl, FormGroup, ControlLabel, HelpBlock, Grid } from 'react-bootstrap'

import * as validations from '../../redux/InterestForm/interestFormValidate'
import { actions, get } from '../../redux/InterestForm/interestFormRedux'

const mapStateToProps = (state, { id }) => {
  const value = get(state, id)
  return {
    value,
    validationState: validations[id](value),
  }
}

const mapDispatchToProps = (dispatch, { id }) => {
  const actionId = _.upperFirst(id)
  return bindActionCreators({
    onChange: actions[`update${actionId}`]
  }, dispatch)
}


class InterestInput extends React.Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.number,
    validationState: PropTypes.bool,
    help: PropTypes.string,
    type: PropTypes.string,
  }

  handleChange = event => {
    this.props.onChange(event.target.value);
  }

  render() {
    const {
      type, label, value, validationState, onChange, help, ...props
    } = this.props;
    return <FormGroup>
      <ControlLabel>{label}</ControlLabel>
      <FormControl
        placeholder     = {`Enter ${label}`}
        onChange        = {this.handleChange}
        validationState = {validationState}
        value           = {value}
        {...{
          type,
          ...props,
        }}
      />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InterestInput)