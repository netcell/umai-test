import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import renderer from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';

import { expectClassname, simulateEvent } from '../../utilities/test-utils'
import configureStore from '../../redux/configureStore'
import { actions } from '../../redux/InterestType/InterestTypeRedux'
import InterestForm from './InterestForm';

describe('InterestForm', () => {

  let component, tree, store;

  const getElement = () => <Provider store={store}>
    <InterestForm />
  </Provider>

  beforeEach(() => {
    store = configureStore()
    component = renderer.create(getElement());
    tree = component.toJSON();
  })
  
  it('4 Fields in compound mode', () => {
    store.dispatch(actions.setModeCompound())
    expect(tree.children[2].children).toHaveLength(4)
  });

  it('3 Fields in simple mode', () => {
    store.dispatch(actions.setModeSimple())
    component.update(getElement())
    tree = component.toJSON();
    expect(tree.children[2].children).toHaveLength(3)
  });
  
})

