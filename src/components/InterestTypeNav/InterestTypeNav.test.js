import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import renderer from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';

import { expectClassname, simulateEvent } from '../../utilities/test-utils'
import configureStore from '../../redux/configureStore'
import InterestTypeNav from './InterestTypeNav';

describe('InterestTypeNav', () => {

  let component, tree;

  beforeEach(() => {
    component = renderer.create(
      <Provider store={configureStore()}>
        <InterestTypeNav />
      </Provider>
    );
    tree = component.toJSON();
  })
  
  it('Start at compound', () => {
    expectClassname(tree.children[0], 'active')
  });

  it('Switch tab on click', () => {
    simulateEvent(tree.children[1].children[0], 'onClick')
    expectClassname(tree.children[0], 'active')
  });
  
})

