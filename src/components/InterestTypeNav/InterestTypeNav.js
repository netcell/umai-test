import _ from 'lodash';
import React from 'react'
import PropTypes from 'prop-types';
import { Nav, NavItem } from 'react-bootstrap'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { actions, get as getInterestType } from '../../redux/InterestType/InterestTypeRedux'
import interestFields from '../../redux/interestFields'
import { COMPOUND, SIMPLE } from '../../consts';

const mapStateToProps = state => {
  const interestType = getInterestType(state);
  return {
    interestType,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setModeCompound: actions.setModeCompound,
    setModeSimple: actions.setModeSimple,
  }, dispatch)
}

class InterestTypeNav extends React.Component {
  
  static propTypes = {
    setModeCompound: PropTypes.func.isRequired,
    setModeSimple: PropTypes.func.isRequired,
    interestType: PropTypes.string.isRequired,
  }

  onSelect = id => {
    if (id == COMPOUND) this.props.setModeCompound()
    else if (id == SIMPLE) this.props.setModeSimple()
  }

  render() {
    const {
      interestType
    } = this.props;
    return <Nav
      bsStyle="tabs"
      justified
      activeKey={interestType}
      onSelect={this.onSelect}
    >
      <NavItem eventKey={COMPOUND}>
        Compound Interest
      </NavItem>
      <NavItem eventKey={SIMPLE}>
        Simple Interest
      </NavItem>
    </Nav>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InterestTypeNav)