import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Table } from 'react-bootstrap'

import { get as getData } from '../../redux/InterestForm/interestFormRedux'
import { get as getType } from '../../redux/InterestType/InterestTypeRedux'
import interestCreator from '../../interestCalculate/interestCreator'

const mapStateToProps = (state, { id }) => {
  const principal         = getData(state, 'principal');
  const rateOfInterest    = getData(state, 'rateOfInterest');
  const compoundFrequency = getData(state, 'compoundFrequency');
  const timePeriod        = getData(state, 'timePeriod');
  const interestType      = getType(state);
  return {
    principal,
    timePeriod,
    interestCalc: interestCreator(interestType)(principal, rateOfInterest, compoundFrequency),
  }
}


class InterestTable extends React.Component {

  static propTypes = {
    principal: PropTypes.number.isRequired,
    timePeriod: PropTypes.number.isRequired,
    interestCalc: PropTypes.func.isRequired,
  }

  renderRow = (time) => {
    const {
      principal, timePeriod, interestCalc
    } = this.props;
    const balance = interestCalc(time);
    /** No problem here since the value is cached with memoize */
    const lastBalance = interestCalc(time - 1);
    return <tr>
      <td>{ time }</td>
      <td>{ (balance - lastBalance).toFixed(2) }</td>
      <td>{ balance.toFixed(2) }</td>
    </tr>
  }

  render() {
    const {
      principal, timePeriod, interestCalc
    } = this.props;
    return <Table responsive>
      <thead>
        <tr>
          <th>Year</th>
          <th>Interest</th>
          <th>Balance</th>
        </tr>
      </thead>
      <tbody>
        { _.range(timePeriod).map(this.renderRow) }
      </tbody>
    </Table>;
  }
}

export default connect(mapStateToProps)(InterestTable)