import compoundInterestCreator from './compoundInterestCreator'
import simpleInterestCreator from './simpleInterestCreator'

import { COMPOUND, SIMPLE } from '../consts'

export default (state) => {
  switch(state) {
    case COMPOUND: return compoundInterestCreator
    case SIMPLE: return simpleInterestCreator
  }
}