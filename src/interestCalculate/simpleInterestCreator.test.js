import simpleInterestCreator from './simpleInterestCreator'

let simpleInterest;

beforeEach(() => {
  simpleInterest = simpleInterestCreator(5000, 5)
})

describe('simpleInterest', () => {
  it('simpleInterest should return principal if time period if 0', () => {
    expect(simpleInterest(0)).toEqual(5000)
  });

  it('simpleInterest should return correct future value if time period if 1', () => {
    expect(simpleInterest(1).toFixed(2)).toEqual('5250.00')
  });

  it('simpleInterest should return correct future value if time period if > 1', () => {
    expect(simpleInterest(12).toFixed(2)).toEqual('8000.00')
  });
})