import compoundInterestCreator from './compoundInterestCreator'

let compoundInterest;

beforeEach(() => {
  compoundInterest = compoundInterestCreator(5000, 5, 12)
})

describe('compoundInterest', () => {
  it('compoundInterest should return principal if time period if 0', () => {
    expect(compoundInterest(0)).toEqual(5000)
  });
  
  it('compoundInterest should return correct future value if time period if 1', () => {
    expect(compoundInterest(1).toFixed(2)).toEqual('5255.81')
  });
  
  it('compoundInterest should return correct future value if time period if > 1', () => {
    expect(compoundInterest(12).toFixed(2)).toEqual('9099.24')
  });
})

