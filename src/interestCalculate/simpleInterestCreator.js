import memoize from 'lodash/memoize'

const simpleInterestCreator = (principal, rateOfInterest) => {
  /** 
   * Calculate the total amount at year $timePeriod providing first year $principal.
   * This is used to display each line of the yearly interest table.
   * The returned value is cached, so multiple calls with the same $timePeriod doesn't affect performance.
   */
  rateOfInterest = rateOfInterest / 100
  return memoize((timePeriod) => {
    return principal * (1 + rateOfInterest * timePeriod)
  })
}

export default simpleInterestCreator;