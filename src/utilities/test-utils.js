export const expectClassname = (element, classname) => {
  return expect(element.props.className.split(' ')).toContain(classname)
}
const mockEvent = { preventDefault: () => {} }
export const simulateEvent = (element, eventFn) => {
  element.props[eventFn](mockEvent)
}