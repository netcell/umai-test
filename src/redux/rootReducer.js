import { combineReducers } from 'redux'
import { reducer as form, name as formName } from './InterestForm/interestFormRedux'
import { reducer as type, name as typeName } from './InterestType/InterestTypeRedux'

const rootReducer = combineReducers({
  [formName]: form,
  [typeName]: type,
})

export default rootReducer