import React from 'react'
import _ from 'lodash'
import { COMPOUND, SIMPLE } from '../consts'

export default (interestType) => _.compact([
  {
    id: 'principal',
    help: null,
    label: 'Principal'
  },
  {
    id: 'rateOfInterest',
    help: null,
    label: 'Rate of interest (%)'
  },
  interestType != COMPOUND ? null : {
    id: 'compoundFrequency',
    help: null,
    label: 'Compound frequency',
    componentClass: 'select',
    children: [
      <option key={1} value={1}>Yearly</option>,
      <option key={2} value={2}>Half Yearly</option>,
      <option key={4} value={4}>Quarterly</option>,
      <option key={12} value={12}>Monthly</option>,
      <option key={365} value={365}>Daily</option>,
    ]
  },
  {
    id: 'timePeriod',
    help: null,
    label: 'Time period (years)'
  },
])