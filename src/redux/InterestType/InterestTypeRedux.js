import { createActions, handleActions, combineActions } from 'redux-actions'

import { COMPOUND, SIMPLE } from '../../consts'

const defaultState = COMPOUND

export const actions = createActions(
  'SET_MODE_COMPOUND',
  'SET_MODE_SIMPLE'
);

const {
  setModeCompound,
  setModeSimple,
} = actions

export const reducer = handleActions({
  [setModeCompound]: (state, action) => COMPOUND,
  [setModeSimple]: (state, action) => SIMPLE,
}, defaultState);

export const name = 'interest_type'

export const get = (state) => state[name]