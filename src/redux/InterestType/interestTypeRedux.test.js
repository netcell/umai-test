import { Reducer } from 'redux-testkit';
import each from 'lodash/each'
import { actions, reducer } from './InterestTypeRedux'
import { COMPOUND, SIMPLE } from '../../consts'

const {
  setModeCompound,
  setModeSimple,
} = actions

const initialState = COMPOUND

describe('interestTypeRedux', () => {
  
  it(`should change to compound`, () => {
    Reducer(reducer)
      .expect(setModeCompound())
      .toReturnState(COMPOUND)
  })

  it(`should change to simple`, () => {
    Reducer(reducer)
      .expect(setModeSimple())
      .toReturnState(SIMPLE)
  })
  
})