import { Reducer } from 'redux-testkit';
import each from 'lodash/each'
import { actions, reducer } from './interestFormRedux'
import * as validations from './interestFormValidate'
import { defaultState } from './interestFormRedux'

const initialState = defaultState

const {
  updatePrincipal,
  updateRateOfInterest,
  updateCompoundFrequency,
  updateTimePeriod,
} = actions

describe('interestFormRedux', () => {
  
  it(`should store principal`, () => {
    const value = 1000
    Reducer(reducer)
      .expect(updatePrincipal(value))
      .toReturnState({
        ...defaultState,
        principal: value
      })
  })
  
  it(`should store rateOfInterest`, () => {
    const value = 5
    Reducer(reducer)
      .expect(updateRateOfInterest(value))
      .toReturnState({
        ...defaultState,
        rateOfInterest: value / 100
      })
  })
  
  it(`should store compoundFrequency`, () => {
    const value = 12
    Reducer(reducer)
      .expect(updateCompoundFrequency(value))
      .toReturnState({
        ...defaultState,
        compoundFrequency: value
      })
  })
  
  it(`should store timePeriod`, () => {
    const value = 5
    Reducer(reducer)
      .expect(updateTimePeriod(value))
      .toReturnState({
        ...defaultState,
        timePeriod: value
      })
  })
  
})

describe('interestFormValidate', () => {
  
  it(`should return null in case of no result`, () => {
    expect(validations.principal()).toBe(null)
    expect(validations.rateOfInterest()).toBe(null)
    expect(validations.compoundFrequency()).toBe(null)
    expect(validations.timePeriod()).toBe(null)
  })
  
})

