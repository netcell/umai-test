import { createActions, handleActions, combineActions } from 'redux-actions'

export const defaultState = {
  principal: 100,
  rateOfInterest: 0.05,
  compoundFrequency: 12,
  timePeriod: 10,
}

export const actions = createActions(
  'UPDATE_PRINCIPAL',
  'UPDATE_RATE_OF_INTEREST',
  'UPDATE_COMPOUND_FREQUENCY',
  'UPDATE_TIME_PERIOD',
);

const {
  updatePrincipal,
  updateRateOfInterest,
  updateCompoundFrequency,
  updateTimePeriod
} = actions

const preprocessPayload = action => {
  if (isNaN(action.payload)) return 0;
  else return Math.max(action.payload, 0);
}

export const reducer = handleActions({
  [updatePrincipal]: (state, action) => ({
    ...state,
    principal: preprocessPayload(action)
  }),
  [updateRateOfInterest]: (state, action) => ({
    ...state,
    rateOfInterest: preprocessPayload(action) * 1 / 100
  }),
  [updateCompoundFrequency]: (state, action) => ({
    ...state,
    compoundFrequency: preprocessPayload(action)
  }),
  [updateTimePeriod]: (state, action) => ({
    ...state,
    timePeriod: preprocessPayload(action)
  })
}, defaultState);

export const name = 'form'

export const get = (state, key) => {
  const value = state[name][key]
  if (key == 'rateOfInterest') return value * 100;
  else return value;
}