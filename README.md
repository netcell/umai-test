# Create React App

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

# Docker

## Testing

```
docker-compose up --build test
```

The container will watch any changes in source code and rerun the test.

## Develop

```
docker-compose up --build start
```

The development server will be served at port `3000`.